<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Pour formater les prix avec la devise par défaut
function prix_declarer_tables_interfaces($interface) {
	$interface['table_des_traitements']['PRIX'][]= 'montant_formater(%s)';
	$interface['table_des_traitements']['PRIX_HT'][]= 'montant_formater(%s)';
	
	return $interface;
}
