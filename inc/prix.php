<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Permet d'obtenir le prix HT d'un objet SPIP.
 * C'est le résultat de cette fonction qui est utilisée pour calculer le prix TTC.
 *
 * @param string $objet
 *   Le type de l'objet
 * @param int $id_objet
 *   L'identifiant de l'objet
 * @param array $options
 *   Tableau d'options :
 *   - arrondi : nombre entier, par défaut celui de la devise.
 *               -1 ou false pour ne pas arrondir.
 *   - serveur
 *   Ou un entier pour l'arrondi pour compat avec l'ancienne signature
 * @param string $serveur
 *   Déprécié. Autre base distante.
 * @return float Retourne le prix HT de l'objet sinon 0
 */
function inc_prix_ht_dist($objet, $id_objet, $options = array(), $serveur = '') {
	include_spip('base/objets');
	include_spip('prix_fonctions'); // pas chargé automatiquement dans certains cas
	include_spip('intl_fonctions'); // pas chargé automatiquement dans certains cas
	$prix_ht = 0;

	// Arrondi de la devise
	$devise = intl_devise_defaut();
	$arrondi_devise = intval(intl_devise_info($devise, 'fraction'));

	// Compatibilité avec l'ancienne signature
	if (is_int($options)) {
		$options = array(
			'arrondi' => $options,
			'serveur' => $serveur,
		);
	}
	// Options par défaut
	$options_defaut = array(
		'arrondi' => $arrondi_devise,
		'serveur' => '',
	);
	// On fusionne avec les défauts
	$options = array_merge($options_defaut, $options);

	// Cherchons d'abord si l'objet existe bien
	if (
		$objet
		and $id_objet = intval($id_objet)
		and $objet = objet_type($objet)
		and $table_sql = table_objet_sql($objet, $options['serveur'])
		and $cle_objet = id_table_objet($objet, $options['serveur'])
		and $ligne = sql_fetsel('*', $table_sql, "$cle_objet = $id_objet", '', '', '', '', $options['serveur'])
	) {
		// Existe-t-il une fonction précise pour le prix HT de ce type d'objet : prix_<objet>_ht() dans prix/<objet>.php
		if ($fonction_ht = charger_fonction('ht', "prix/$objet", true)) {
			// On passe la ligne SQL en paramètre pour ne pas refaire la requête
			$prix_ht = $fonction_ht($id_objet, $ligne, $options);
		}
		// S'il n'y a pas de fonction, regardons s'il existe des champs normalisés, ce qui évite d'écrire une fonction pour rien
		elseif (!empty($ligne['prix_ht'])) {
			$prix_ht = $ligne['prix_ht'];
		}
		elseif (!empty($ligne['prix'])) {
			$prix_ht = $ligne['prix'];
		}

		// On passe dans un pipeline pour modifier le prix HT
		$prix_ht = pipeline(
			'prix_ht',
			array(
				'args' => array(
					'objet' => $objet,
					'id_objet' => $id_objet,
					'type_objet' => $objet, // déprécié, utiliser plutôt "objet"
					'prix_ht' => $prix_ht,
					'options' => $options,
					'ligne' => $ligne,
				),
				'data' => $prix_ht
			)
		);
	}

	// Enfin si nécessaire, on fait un arrondi
	if (
		is_int($options['arrondi'])
		and $options['arrondi'] >= 0
	) {
		$prix_ht = round($prix_ht, $options['arrondi']);
	}

	return $prix_ht;
}

/**
 * Permet d'obtenir le prix final TTC d'un objet SPIP quel qu'il soit.
 *
 * @param string $objet
 *   Le type de l'objet
 * @param int $id_objet
 *   L'identifiant de l'objet
 * @param array $options
 *   Tableau d'options :
 *   - arrondi : nombre entier, par défaut celui de la devise.
 *               -1 ou false pour ne pas arrondir.
 *   - serveur
 *   Ou un entier pour l'arrondi pour compat avec l'ancienne signature
 * @param string $serveur
 *   Déprécié. Autre base distante.
 * @return float Retourne le prix TTC de l'objet sinon 0
 */
function inc_prix_dist($objet, $id_objet, $options = array(), $serveur = '') {
	include_spip('base/objets');
	include_spip('prix_fonctions'); // pas chargé automatiquement dans certains cas
	include_spip('intl_fonctions'); // pas chargé automatiquement dans certains cas
	
	// Arrondi de la devise
	$devise = intl_devise_defaut();
	$arrondi_devise = intval(intl_devise_info($devise, 'fraction'));

	// Compatibilité avec l'ancienne signature
	if (is_int($options)) {
		$options = array(
			'arrondi' => $options,
			'serveur' => $serveur,
		);
	}
	// Options par défaut
	$options_defaut = array(
		'arrondi' => $arrondi_devise,
		'serveur' => '',
	);
	// On fusionne avec les défauts
	$options = array_merge($options_defaut, $options);

	// On va d'abord chercher le prix HT. On délègue le test de présence de l'objet dans cette fonction.
	$fonction_prix_ht = charger_fonction('ht', 'inc/prix');
	$objet = objet_type($objet);
	$options_ht = array_merge($options, array('arrondi' => false));
	$prix = $prix_ht = $fonction_prix_ht($objet, $id_objet, $options_ht);
	$taxes = array();

	// On cherche maintenant s'il existe une personnalisation pour le prix total TTC : prix_<objet>() dans prix/<objet>.php
	if ($fonction_prix_objet = charger_fonction($objet, 'prix/', true)) {
		$prix = $fonction_prix_objet($id_objet, $prix_ht, $options);
	}
	// Sinon on appelle une fonction générique pour trouver les taxes d'un objet, et on ajoute au HT
	elseif ($fonction_taxes = charger_fonction('taxes', 'inc/', true)) {
		$taxes = $fonction_taxes($objet, $id_objet, $options);
		$taxes_total = array_sum(array_column($taxes, 'montant'));
		$prix = $prix_ht + $taxes_total;
	}

	// On passe dans un pipeline pour pouvoir ajouter taxes, ristournes ou autres modifications
	$prix = pipeline(
		'prix',
		array(
			'args' => array(
				'objet' => $objet,
				'id_objet' => $id_objet,
				'type_objet' => $objet, // déprécié, utiliser plutôt "objet"
				'prix_ht' => $prix_ht,
				'options' => $options,
				'taxes' => $taxes,
			),
			'data' => $prix
		)
	);

	// Enfin si nécessaire, on fait un arrondi
	if (
		is_int($options['arrondi'])
		and $options['arrondi'] >= 0
	) {
		$prix = round($prix, $options['arrondi']);
	}

	// Et c'est fini
	return $prix;
}
