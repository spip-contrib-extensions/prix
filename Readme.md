# Plugin API Prix

> Interface de programmation pour connaître le prix d’un objet SPIP. Ce plugin est un outil pour développeur.

* Documentation de la v0.x : https://contrib.spip.net/Plugin-API-Prix
* Documentation de la v1.x : à faire. Voir les [notes de versions](Changelog.md) en attendant.