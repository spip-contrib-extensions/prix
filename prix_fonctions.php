<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * La balise qui va avec le prix TTC
 *
 * @param Object $p
 * @return Float
 */
function balise_PRIX_dist($p) {
	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if (!$_type = interprete_argument_balise(1,$p)){
		$_type = sql_quote($p->boucles[$b]->type_requete);
		$_id = champ_sql($p->boucles[$b]->primary,$p);
	}
	else
		$_id = interprete_argument_balise(2,$p);

	$connect = ($b ? $p->boucles[$b]->sql_serveur : '');
	if (!$options = interprete_argument_balise(3,$p)) {
		$options = "[]";
	}
	$p->code = "prix_objet(intval(".$_id."),".$_type.','.$options.','.sql_quote($connect).")";
	$p->interdire_scripts = false;

	return $p;
}

/**
 * La balise qui va avec le prix HT
 *
 * @param Object $p
 * @return Float
 */
function balise_PRIX_HT_dist($p) {
	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if (!$_type = interprete_argument_balise(1,$p)){
		$_type = sql_quote($p->boucles[$b]->type_requete);
		$_id = champ_sql($p->boucles[$b]->primary,$p);
	}
	else
		$_id = interprete_argument_balise(2,$p);
	$connect = ($b ? $p->boucles[$b]->sql_serveur : '');
	if (!$options = interprete_argument_balise(3,$p)) {
		$options = "[]";
	}
	$p->code = "prix_ht_objet(intval(".$_id."),".$_type.','.$options.','.sql_quote($connect).")";
	$p->interdire_scripts = false;
	return $p;
}

/**
 * Obtenir le prix TTC d'un objet
 *
 * @param Integer $id_objet
 * @param String $objet
 * @return Float
 */
function prix_objet($id_objet, $objet, $options = [], $serveur = '') {
	$fonction = charger_fonction('prix', 'inc/');
	return $fonction($objet, $id_objet, $options, $serveur);
}

/**
 * Obtenir le prix HT d'un objet
 *
 * @param Integer $id_objet
 * @param String $objet
 * @return Float
 */
function prix_ht_objet($id_objet, $objet, $options = []) {
	$fonction = charger_fonction('ht', 'inc/prix');
	return $fonction($objet, $id_objet, $options);
}

/**
 * Compatibilité avec la balise #INFO_PRIX
 *
 * @uses prix_objet
 *
 * @param Integer $id_objet
 * @param String $objet
 * @param Array $ligne
 * @return Float
 */
function generer_prix_entite($id_objet, $objet, $ligne) {
	return prix_objet($id_objet, $objet);
}

/**
 * Compatibilité avec la balise #INFO_PRIX_HT
 *
 * @uses prix_ht_objet
 *
 * @param Integer $id_objet
 * @param String $objet
 * @param Array $ligne
 * @return Float
 */
function generer_prix_ht_entite($id_objet, $objet, $ligne) {
	return prix_ht_objet($id_objet, $objet);
}

/**
 * Ancienne fonction dépréciée pour formater un montant
 *
 * @note
 * Fonction déportée dans la fonction Intl `filtre_montant_formater_dist`.
 *
 * @uses filtre_montant_formater_dist
 */
function prix_formater($montant, $options = array()) {
	include_spip('inc/filtres');
	include_spip('intl_fonctions');

	$fonction_formater = chercher_filtre('montant_formater');

	return $fonction_formater($montant, $options);
}

/**
 * Ancienne fonction dépréciée pour formater un montant
 *
 * @note
 * Fonction déportée dans la fonction Intl `filtre_montant_formater_dist`.
 *
 * @uses filtre_montant_formater_dist
 */
function filtres_prix_formater_dist($montant, $options = array()) {
	include_spip('inc/filtres');
	include_spip('intl_fonctions');

	$fonction_formater = chercher_filtre('montant_formater');

	return $fonction_formater($montant, $options);
}

