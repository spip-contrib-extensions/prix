<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_voir_demo' => 'Página de demonstração',

	// L
	'label_prix_ht' => 'Preço sem IVA',
	'label_prix_ttc' => 'Preço com IVA',
	'label_taxes' => 'Impostos',
	'label_total_ht' => 'Total sem IVA',
	'label_total_ttc' => 'Total com IVA',

	// P
	'prix_ht' => '@prix@ sem IVA',
	'prix_ttc' => '@prix@ com IVA',

	// T
	'titre_page_configurer' => 'Configurar preço',
	'titre_prix' => 'Preço'
);
