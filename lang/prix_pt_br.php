<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_voir_demo' => 'Página de demonstração',

	// L
	'label_prix_ht' => 'Preço sem impostos',
	'label_prix_ttc' => 'Preço com todos os impostos',
	'label_taxes' => 'Taxas',
	'label_total_ht' => 'Total sem impostos',
	'label_total_ttc' => 'Total com todos os impostos',

	// P
	'prix_ht' => '@prix@ sem impostos',
	'prix_ttc' => '@prix@ com todos os impostos',

	// T
	'titre_page_configurer' => 'Configurar Preço',
	'titre_prix' => 'Preço'
);
