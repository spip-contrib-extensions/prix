<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_voir_demo' => 'デモページ',

	// L
	'label_prix_ht' => '税別価格',
	'label_prix_ttc' => '税込み価格',
	'label_taxes' => '税',
	'label_total_ht' => '税別合計',
	'label_total_ttc' => '税込み合計',

	// P
	'prix_ht' => '@prix@ 税別',
	'prix_ttc' => '@prix@ 税込み',

	// T
	'titre_page_configurer' => '通貨を設定',
	'titre_prix' => '価格'
);
