<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_voir_demo' => 'Página de demostración', # RELIRE

	// L
	'label_prix_ht' => 'Precio excl.', # RELIRE
	'label_prix_ttc' => 'Precio incl.', # RELIRE
	'label_taxes' => 'Impuestos', # RELIRE
	'label_total_ttc' => 'Precio total', # RELIRE

	// P
	'prix_ht' => '@prix@ excl.', # RELIRE
	'prix_ttc' => '@prix@ incl.', # RELIRE

	// T
	'titre_page_configurer' => 'Configurar Precio', # RELIRE
	'titre_prix' => 'Precio' # RELIRE
);
