<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'icone_voir_demo' => 'Demo page',

	// L
	'label_prix_ht' => 'ET price',
	'label_prix_ttc' => 'ATI price',
	'label_taxes' => 'Taxes',
	'label_total_ht' => 'Total ET',
	'label_total_ttc' => 'Total ATI',

	// P
	'prix_ht' => '@prix@ ET',
	'prix_ttc' => '@prix@ ATI',

	// T
	'titre_page_configurer' => 'Configure Prices',
	'titre_prix' => 'Price'
);
