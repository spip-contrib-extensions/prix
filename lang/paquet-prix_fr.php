<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/prix.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'prix_description' => 'Interface de programmation pour connaître le prix d’un objet SPIP. Ce plugin est un outil de développement.',
	'prix_nom' => 'API Prix',
	'prix_slogan' => 'API pour connaître le prix d’un objet'
);
