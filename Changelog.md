# Plugin API Prix : notes de versions

## v1.x

* Gestion des prix simplifiée : les cas simples sont automatiquement pris en compte. Si l'objet possède des champs `prix_ht` (ou `prix`) et `taxe`, alors il n'est plus nécessaire de créer des fichiers contenant des fonctions personnalisées dans `prix/<objet>.php`.
* Gestion des taxes étendue : possibilité de personnaliser les taxes appliquées à un objet en créant une fonction `taxes_<objet>()` dans `taxes/<objet>.php` ou au moyen d'un nouveau pipeline `taxes` (voir [inc/taxes.php#L41](inc/taxes.php) pour les paramètres).
* La devise principale devient configurable dans l’espace privé (cf. constantes dépréciées).
* Amélioration du formatage des prix : prise en compte de toutes les locales et devises. Le formatage est confié à la librairie php [Intl de Commerceguys]((https://github.com/commerceguys/intl/)). De multiples options de formatage sont possibles : affichage comptable, choix entre le symbole ou le code pour la devise, etc. Liste des options dans [prix_fonctions.php#L139](prix_fonctions.php).
* Ajout de pages de config et de démo.
* Compatibilité avec les balises `#INFO_PRIX` et `#INFO_PRIX_HT`

**Déprécations**

* Constante `PRIX_DEVISE` : remplacée par une option de configuration
* Constante `DEVISE_DEFAUT` : remplacée par une option de configuration